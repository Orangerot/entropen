import fetch from 'node-fetch';

class StateGetter {
    last = true;
    state = false;

    // gets status; return true while status does not change
    async getStatus() {
        const response = await fetch( "https://club.entropia.de/status.json" );
        const json = await response.json();
        this.last = this.state;
        this.state = json;
        return this.state;
    }

    didChange() {
        return !this.last || (this.last.club_offen != this.state.club_offen);
    }
}

export default StateGetter;
