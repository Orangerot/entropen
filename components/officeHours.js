import fetch from 'node-fetch';

async function officeHours(isOpen) {
    const response = await fetch( "https://mapall.space/heatmap//json.php?id=Entropia");
    const json = await response.json();
    const date = new Date();
    const now = (date.getDay()-1)%7 * 24 + date.getHours();

    let list = Object.values(json.data).map(e => 
        Object.values(e).slice(0, 24)
    ).slice(0,7).flat();    
    list = [...list, ...list];
    
    let nextClose = 0;
    let nextOpen = 0;

    if (isOpen) {
        nextClose = list.findIndex((t,i) => i > now && t <= .5);
        nextOpen = list.findIndex((t,i) => i > nextClose && t >= .5) % (7*24);
    } else {
        nextOpen = list.findIndex((t,i) => i >= now && t >= .5);
        nextClose = list.findIndex((t,i) => i > nextOpen && t <= .5) % (7*24);
    }

    return [nextClose, nextOpen];
 }

export default officeHours;
