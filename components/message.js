import officeHours from "./officeHours.js";

async function message({club_offen: isOpen}) {
    const week = [
        "Montag",
        "Dienstag",
        "Mittwoch",
        "Donnerstag",
        "Freitag",
        "Samstag",
        "Sonntag"
    ];

    const [close, open] = await officeHours(isOpen);
    const eventMsg = (time, event) =>  
        `Nächste geschätzte ${event ? "Öffnung" : "Schließung"}: ` + 
        `<em>${week[Math.floor(time/24)]}, ${time % 24}:00</em>`;

    const msg = `
        <p>
            <strong>Der Club ist ${isOpen ? "offen" : "zu"}.</strong><br>
            ${eventMsg(Math.min(close, open), open < close) + "<br>" + 
                eventMsg(Math.max(close, open), open > close)}
        </p>
    `;

    return msg;
}

export default message;
