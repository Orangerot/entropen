import {
    MatrixClient,
    SimpleFsStorageProvider,
    RustSdkCryptoStorageProvider,
    AutojoinRoomsMixin
} from "matrix-bot-sdk";
import * as dotenv from "dotenv";

import { htmlToText } from "html-to-text";
import StateGetter from "./components/state.js";
import message from "./components/message.js";
dotenv.config();

// !!! create a '.env' file with MATRIX_PROVEDER=... and MATRIX_ACCESS_TOKEN=...
const homeserverUrl = process.env.MATRIX_PROVIDER; 
const accessToken = process.env.MATRIX_ACCESS_TOKEN;
const storage = new SimpleFsStorageProvider("bot.json");
// const crypt = new RustSdkCryptoStorageProvider("crypt");

// const client = new MatrixClient(homeserverUrl, accessToken, storage, crypt);
const client = new MatrixClient(homeserverUrl, accessToken, storage);
AutojoinRoomsMixin.setupOnClient(client);

const openLogo = "mxc://matrix.org/SNHLYqtVldbtxQIZLBztjiEf";
const closeLogo = "mxc://matrix.org/pNdScxpkpOcWUnEsyXvpYAwk";
const space = new StateGetter();

async function sendStatus(room, msg) {
    console.log((new Date).toUTCString(), "send status to room", room);
    if (await client.userHasPowerLevelFor(await client.getUserId(), room, "m.room.topic", true)) {
        client.sendStateEvent(room, "m.room.topic", "", {topic: htmlToText(msg)});
    } else {
        client.sendHtmlText(room, msg);
    }
}

client.start().then(() => console.log("Client started!"));

// greet with the status on joining a room
client.on("room.join", async (roomId, _event) => {
    sendStatus(roomId, await message(await space.getStatus()));
});

// answer status on "open?" command
client.on("room.message", async (roomId, event) => {
    const {content, sender} = event;
    if (content?.msgtype !== 'm.text' || 
        sender === await client.getUserId() || 
        !content.body?.startsWith("open?")
    ) return;

    // Now that we've passed all the checks, we can actually act upon the command
    console.log((new Date()).toUTCString(), roomId, "asked");
    client.replyHtmlNotice(roomId, event, await message(await space.getStatus()));
});

// broadcasts to all chanels if there was a change 
setInterval(async () => {
    const state = await space.getStatus();
    if (!space.didChange()) return;

    client.setAvatarUrl( state.club_offen ? openLogo : closeLogo );

    const msg = await message(state);
    (await client.getJoinedRooms()).forEach(room => sendStatus(room, msg));
}, 10000);
