# Club-Status Matrix-Bot

> Notify a matrix room when a hackerspace opens/closes

## Features

- [X] Text message notification
- [X] Change room topic
- [X] get probable office hours
- [X] change profile picture (avatar)
- [X] respond to chat commands (`open?`)

## Usage

Invite or DM the bot to your room. You can give it the permission to set the
room topic. From now on you will receive status updates. You also can always
write `open?` to receive a direct answer. 

## Getting Started

### Creating a Bot

Create a new matrix account in a private browser window. On element navigate to 
`Setting -> Help and About -> Extended -> Access Token`, copy it and CLOSE THE
WINDOW. See https://t2bot.io/docs/access_tokens/

### Installation

> Node.js and npm are required. 

```sh
https://gitlab.com/Orangerot/entropen.git
cd entropen
npm install
```
Create a `.env` file with the following content:

```
MATRIX_ACCESS_TOKEN=your_access_token
MATRIX_PROVIDER=https://matrix.org (or your privider)
```

Start the bot with `npm run bot`. 

## Technology

- [matrix-bot-sdk](https://github.com/turt2live/matrix-bot-sdk)
- [SpaceAPI](https://spaceapi.io/)
- https://mapall.space/

## License

This project is licensed under the GPLv3 License - see the `LICENSE` file for
details.
